export PATH="$HOME/.cargo/bin/:$PATH"
alias vim="nvim"
alias python="python3"
alias tmux="tmux -u"
. $HOME/code/tools/anaconda3/etc/profile.d/conda.sh
alias zshrc="vim $HOME/.zshrc && source $HOME/.zshrc"
alias zshenv="vim $HOME/.zshenv && source $HOME/.zshenv"
export VIRTUAL_ENV_DISABLE_PROMPT=
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow --glob "!.git/*"'

export TERM=xterm-256color
set -o vi
export EDITOR=vim
set editing-mode vi
set keymap vi-command
export VIRTUAL_ENV_DISABLE_PROMPT=
#export PYTHONSTARTUP=~/.pythonrc.py
export NODE_TLS_REJECT_UNAUTHORIZED=0
export PYTHONDONTWRITEBYTECODE=1
alias open="xdg-open"

export PATH="$PATH:/snap/bin/"
alias desktop="ssh gaudy"

alias fd="fdfind"
export PATH="$HOME/code/tools/racketlang/bin:$PATH"

