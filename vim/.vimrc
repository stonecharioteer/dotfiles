" set leader to `,`
let mapleader = ","
" turn on line numbers
set number
" enable syntax highlighting
syntax on
" enable ruler
set ruler
" highlight search
set hls
" Set tabs to 4 spaces
set tabstop=4
set softtabstop=4
set expandtab

set cursorline
filetype indent on
set wildmenu
set lazyredraw
set showmatch
set incsearch
nnoremap <leader>s :nohlsearch<CR>
set foldenable
set foldlevelstart=10
set foldnestmax=10
noremap <space> za
set foldmethod=indent

" set whitespace characters
" set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣
" set list

set listchars=tab:->,space:·
nnoremap <leader><space> :set list!<CR>

" nnoremap j gj
" noremap k gk

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'https://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

Plugin 'davidhalter/jedi-vim'
Plugin 'preservim/nerdtree'
Plugin 'airblade/vim-gitgutter'
Plugin 'vim-airline/vim-airline'
Plugin 'scrooloose/nerdcommenter'
Plugin 'honza/vim-snippets'
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
Plugin 'gilsondev/searchtasks.vim'
Plugin 'irrationalistic/vim-tasks'
Plugin 'easymotion/vim-easymotion'
Plugin 'rainglow/vim'
Plugin 'yggdroot/indentline'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'craigemery/vim-autotag'
Plugin 'Valloric/YouCompleteMe'
Plugin 'vim-syntastic/syntastic'
Plugin 'nvie/vim-flake8'
Plugin 'kien/ctrlp.vim'
Plugin 'mhinz/vim-startify'
Plugin 'apzelos/blamer.nvim'
Plugin 'dart-lang/dart-vim-plugin'
Plugin 'natebosch/vim-lsc'
Plugin 'natebosch/vim-lsc-dart'
Plugin 'rust-lang/rust.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


nnoremap <C-n> :NERDTreeToggle<CR>
" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

" Remap movements
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" use natural splits

set splitbelow
set splitright

" <Leader>f{char} to move to {char}
noremap  <Leader>f <Plug>(easymotion-bd-f)
nnoremap <Leader>f <Plug>(easymotion-overwin-f)

" s{char}{char} to move to {char}{char}
nnoremap s <Plug>(easymotion-overwin-f2)

" Move to line
noremap <Leader>L <Plug>(easymotion-bd-jk)
nnoremap <Leader>L <Plug>(easymotion-overwin-line)

" Move to word
noremap  <Leader>w <Plug>(easymotion-bd-w)
nnoremap <Leader>w <Plug>(easymotion-overwin-w)

" split screens
nnoremap <leader>\| :vsplit<CR>
nnoremap <leader>- :split<CR>

" open vimrc easily
nnoremap <leader>ev :vsplit $MYVIMRC<cr>
" source vimrc
nnoremap <leader>sv :source $MYVIMRC<cr>

" disable arrow keys
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
" inoremap <esc> <nop>
" inoremap jk <esc>
" noremap <leader>h <ctrl>wh
" noremap <leader>j <ctrl>wj
" noremap <leader>k <ctrl>wk
" noremap <leader>l <ctrl>wl
"
hi CursorLine   ctermbg=darkred ctermfg=white guibg=darkred guifg=white
hi CursorColumn ctermbg=darkred ctermfg=white guibg=darkred guifg=white

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" fix python indentation

au BufNewFile,BufRead *.py
     \ set tabstop=4
     \ set softtabstop=4
     \ set shiftwidth=4
     \ set textwidth=79
     \ set expandtab
     \ set autoindent
     \ set fileformat=unix

au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2

" au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

set encoding=utf-8

let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

let g:python3_host_prog = '/home/stonecharioteer/.nvimenv/bin/python3'
let g:loaded_python_provider = 0

" python with virtualenv support
let python_highlight_all=1
syntax on

let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

set guifont=Hermit\ Medium:h14
"set guifont=Menlo\ Regular:h14

nmap <leader>L <C-w>L
nmap <leader>H <C-w>H
nmap <leader>J <C-w>J
nmap <leader>K <C-w>K

py3 << EOF
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
  project_base_dir = os.environ['VIRTUAL_ENV']
  activate_this = os.path.join(project_base_dir, 'bin/activate')
  os.system(activate_this)
  #  exec(activate_this, dict(__file__=activate_this))
EOF

