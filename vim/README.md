# Using Vim

I've completely moved to nvim, abandoning VS Code. I find the terminal calming, and I don't want to rely on a GUI-based tool anymore.

This vimrc contains all my plugins and configuration.


## Installation

```bash

sudo apt-get install neovim
# brew install neovim for mac

git clone https://gitlab.com/stonecharioteer/dotfiles ~/.dotfiles

ln -s $HOME/.dotfiles/vim/.vimrc $HOME/.vimrc 
mkdir -p ~/.config/nvim/
ln -s $HOME/.dotfiles/vim/nvim/init.vim $HOME/.config/nvim/init.vim
conda activate base # Or use some viable python3.6+
python3 -m venv ~/.nvimenv
~/.nvimenv/bin/pip install neovim
alias nvim='vim'
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
# or open vim and run :PluginInstall
```

To Setup YouCompleteMe,

```bash
cd ~/.vim/bundle/YouCompleteMe
./install.py --all
```

For Macs, ensure you install macvim and use the binary packaged along with it instead of the system's native vim.
