#!/bin/bash

nord="$(nordvpn status | grep City | cut -c 7-)"


if [ "$nord" == '' ]
then
        echo "VPN: Disconnected"
else
        echo "VPN: $nord"
fi

