#!/bin/zsh
#USERNAME=<>
#PASSWORD=<>
httpproxy='http://$USERNAME:$PASSWORD@proxy.com'
httpsproxy='https://$USERNAME:$PASSWORD@proxy.com'

function toggle_proxies() {
    if [ -z "$http_proxy" ]; then
        export http_proxy="$httpproxy"
        export HTTP_PROXY="$httpproxy"
        export https_proxy="$httpsproxy"
        export HTTPS_PROXY="$https_proxy"
        export NO_PROXY="localhost,127.0.0.1"
    else
        echo 'Unsetting proxies.'
        unset http_proxy
        unset HTTP_PROXY
        unset https_proxy
        unset HTTPS_PROXY
   fi
}



function set_npm_conf() {
    #npm config set registry <registry>
    #npm config set SASS_BINARY_SITE <node-sass>
    npm config set cafile $HOME/ca-bundle.crt
    npm config set proxy $httpproxy
    npm config set https-proxy $httpsproxy
}

function rm_npm_conf() {
    npm config delete proxy
    npm config delete https-proxy
    npm config delete cafile
    npm config delete SASS_BINARY_SITE
    npm config delete registry
}

function set_yarn_conf() {
    yarn config set proxy $httpproxy;
    yarn config set https-proxy $httpsproxy;
    #yarn config set SASS_BINARY_SITE <node-sass>;
    yarn config set cafile $HOME/ca-bundle.crt;
    yarn config set registry <registry>;

}

function rm_yarn_conf() {
    yarn config delete proxy;
    yarn config delete https-proxy;
    yarn config delete cafile;
    yarn config delete registry;
    yarn config delete SASS_BINARY_SITE;
}

function set_pip_conf() {
    pip config set global.ssl-verify false
    # Use this for all repos if you need.
    #pip config set global.index-url <registry>
    pip config set global.disable-pip-version-check true
    #pip config set global.trusted-host <registry-host>
    pip config set global.cert $HOME/ca-bundle.crt
}

function rm_pip_conf() {
    pip config set global.ssl-verify true
    # pip config set user.index-url <registry>
    pip config unset global.index-url
    pip config unset global.disable-pip-version-check
    pip config unset global.trusted-host
    pip config unset global.cert
}

function set_conda_conf() {
    #conda config --add channels https://<registry>
    #conda config --add channels https://$USERNAME:$PASSWORD@<registry>
    conda config --set ssl_verify $HOME/ca-bundle.crt
    #conda config --add default_channels https://$USERNAME:$PASSWORD@<registry>
    conda config --set always_yes true
    conda config --set show_channel_urls true
    conda config --set auto_update_conda false
}

function rm_conda_conf() {
    conda config --remove-key channels
    conda config --remove-key channel_alias
    conda config --remove-key default_channels
    conda config --set ssl_verify true
    conda config --set always_yes true
    conda config --set show_channel_urls true
    conda config --set auto_update_conda false
}


function public_wifi() {
        networksetup -setairportnetwork en0 'InsidiousInternet_5G' 2>&1 >> /dev/null
        sleep 2
        networksetup -setairportpower airport off 2>&1 >> /dev/null
        networksetup -setairportpower airport on 2>&1 >> /dev/null
        prompt
}

function visa_wifi() {
    networksetup -setairportnetwork en0 'PintSizeNet' 2>&1 >> /dev/null
    sleep 2
    networksetup -setairportpower airport off 2>&1 >> /dev/null
    networksetup -setairportpower airport on 2>&1 >> /dev/null
    prompt
}

function prompt() {
    export wifi_ssid=`/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I | grep -i " SSID" | cut -d ':' -f 2 | awk '{ sub(/^[ \t]+/, ""); print }'`

    PROMPT='%F{red}%n%f@%F{blue}%m%f %F{yellow}%d%f %$'$'\n'"[%F{yellow}%?%f]${ret_status} %{$fg[yellow]%}%c%{$reset_color%}"$'\n📶[$wifi_ssid]\n$(git_prompt_info)%F{yellow}\$ %{$reset_color%}'
}
